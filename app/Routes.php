<?php
/**
 * Avalon - Routes.php
 *
 * @author Alexis 'Neziaa'
 * @version 1.2.0 - BETA
 * @Copyright Avalon © 2016-2018 - https://avalon.mc
 *
 */

use \Slim\Http\Request;
use \Slim\Http\Response;

$app->get('/', function(Request $request, Response $response) {
    return $response->getBody()->write('salam');
});