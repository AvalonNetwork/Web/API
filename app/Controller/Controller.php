<?php
/**
 * Avalon - Controller.php
 *
 * @author Alexis 'Neziaa'
 * @version 1.2.0 - BETA
 * @Copyright Avalon © 2016-2018 - https://avalon.mc
 *
 */

namespace App\Controller;

use Interop\Container\ContainerInterface;

class Controller {

    protected $container;

    public function __construct(ContainerInterface $ci) {
        $this->container = $ci;
    }

}