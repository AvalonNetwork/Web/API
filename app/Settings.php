<?php
/**
 * Avalon - Settings.php
 *
 * @author Alexis 'Neziaa'
 * @version 1.2.0 - BETA
 * @Copyright Avalon © 2016-2018 - https://avalon.mc
 *
 */

return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,

        'database' => [
            'hostname' => 'localhost',
            'port' => '3307',
            'database' => 'api',
            'username' => 'root',
            'password' => 'mariadb',
            'prefix'   => '',
        ],
    ],
];
