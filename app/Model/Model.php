<?php
/**
 * Avalon - Model.php
 *
 * @author Alexis 'Neziaa'
 * @version 1.2.0 - BETA
 * @Copyright Avalon © 2016-2018 - https://avalon.mc
 *
 */
namespace App\Model;

class Model extends \Illuminate\Database\Eloquent\Model {

}