<?php
/**
 * Avalon - index.php
 *
 * @author Alexis 'Neziaa'
 * @version 1.2.0 - BETA
 * @Copyright Avalon © 2016-2018 - https://avalon.mc
 *
 */

require 'vendor/autoload.php';

session_start();

$settings = require __DIR__ . '/app/Settings.php';
$app = new \Slim\App($settings);

require __DIR__ . '/app/Functions.php';
require __DIR__ . '/app/Dependencies.php';
require __DIR__ . '/app/Routes.php';
require __DIR__ . '/app/Middleware.php';

$app->run();